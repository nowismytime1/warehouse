package main

// File paths
const (
	InventoryPath = "./inventory.json"
	ProductsPath  = "./products.json"
)

// Colours
const (
	ColorGreen  = "\033[32m"
	ColorRed    = "\033[31m"
	ColorYellow = "\033[33m"
	ColorBlue   = "\033[34m"
)

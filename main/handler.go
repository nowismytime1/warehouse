package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// InitializeFromFile method is called at startup to load initial data from disk
func (w *Warehouse) InitializeFromFile() error {
	// Loading inventory from disk
	err := w.LoadInventoryController(InventoryPath, false)
	if err != nil {
		return err
	}

	err = w.LoadProductCatalogueController(ProductsPath)
	if err != nil {
		return err
	}

	// calculating number of products based on change in product catalogue
	w.MaxAvailableProducts = w.FindMaxAvailableProducts()
	fmt.Println(ColorBlue, "Successfully loaded inventory and product catalogue from disk at startup")
	fmt.Println()
	return nil
}

// ListProductsInInventory lists maximum available products
func (w *Warehouse) ListProductsInInventory() {
	for productName, quantity := range w.MaxAvailableProducts {
		fmt.Println(ColorBlue, fmt.Sprintf("Product Name: %s; Price: %f; Available Quantity: %d;",
			productName, w.ProductCatalogue[productName].Price, quantity))
	}
	fmt.Println()
}

// ListSoldProducts lists products that have been sold in this session
func (w *Warehouse) ListSoldProducts() {
	totalSales := float64(0)
	for productName, quantitySold := range w.ProductsSold {
		fmt.Println(ColorBlue, fmt.Sprintf("Product Name: %s; Price: %f; Quantity Sold: %d;",
			productName, w.ProductCatalogue[productName].Price, quantitySold))
		totalSales += w.ProductCatalogue[productName].Price * float64(quantitySold)
	}
	fmt.Println(ColorBlue, fmt.Sprintf("Total Sales in this session: %f", totalSales))
	fmt.Println()
}

// LoadProductCatalogue loads product catalogue from the input file
func (w *Warehouse) LoadProductCatalogue() error {
	// Reading input path
	fmt.Println(ColorGreen, "Please enter the input file path")
	var inputFilePath string
	_, err := fmt.Scanln(&inputFilePath)
	if err != nil {
		return errors.New(fmt.Sprintf("Sorry. There was an error while reading the input: %v", err))
	}

	// Calling controller method
	err = w.LoadProductCatalogueController(inputFilePath)
	if err != nil {
		return err
	}

	// recalculating number of products based on change in product catalogue
	w.MaxAvailableProducts = w.FindMaxAvailableProducts()
	fmt.Println(ColorBlue, "Successfully loaded product catalogue from file")
	fmt.Println()
	return nil
}

// LoadFreshInventory loads fresh inventory from the input file
func (w *Warehouse) LoadFreshInventory() error {
	// Reading input path
	fmt.Println(ColorGreen, "Please enter the input file path")
	var inputFilePath string
	_, err := fmt.Scanln(&inputFilePath)
	if err != nil {
		return errors.New(fmt.Sprintf("Sorry. There was an error while reading the input: %v", err))
	}

	// Calling controller method
	err = w.LoadInventoryController(inputFilePath, false)
	if err != nil {
		return err
	}

	// recalculating number of products based on change in inventory
	w.MaxAvailableProducts = w.FindMaxAvailableProducts()
	fmt.Println(ColorBlue, "Successfully loaded fresh inventory from file")
	fmt.Println()
	return nil
}

// LoadFreshInventory loads incremental inventory from the input file
func (w *Warehouse) LoadIncrementalInventory() error {
	// Reading input path
	fmt.Println(ColorGreen, "Please enter the input file path")
	var inputFilePath string
	_, err := fmt.Scanln(&inputFilePath)
	if err != nil {
		return errors.New(fmt.Sprintf("Sorry. There was an error while reading the input: %v", err))
	}

	// Calling controller method
	err = w.LoadInventoryController(inputFilePath, true)
	if err != nil {
		return err
	}

	// recalculating number of products based on change in inventory
	w.MaxAvailableProducts = w.FindMaxAvailableProducts()
	fmt.Println(ColorBlue, "Successfully loaded fresh inventory from file")
	fmt.Println()
	return nil
}

// SaveOnExit saves the current inventory and product catalogue on exit
func (w *Warehouse) SaveOnExit() error {
	// saving inventory
	var inventory InventoryModel
	for _, article := range w.Inventory {
		inventory.Articles = append(inventory.Articles, MapArticleToModel(article))
	}

	err := w.SaveJSONToFile(inventory, InventoryPath)
	if err != nil {
		return err
	}

	// saving products
	var productCatalogue ProductCatalogueModel
	for _, product := range w.ProductCatalogue {
		productCatalogue.Products = append(productCatalogue.Products, MapProductToModel(product))
	}

	err = w.SaveJSONToFile(inventory, InventoryPath)
	if err != nil {
		return err
	}

	fmt.Println(ColorBlue, "Successfully saved. Exiting now...")
	fmt.Println()
	return nil
}

// SellProduct can be used to sell some quantity of a product
func (w *Warehouse) SellProduct() error {
	// getting name of the product and quantity from user
	name, quantity, err := getNameAndQuantityFromUser(w.ProductCatalogue)
	if err != nil {
		return err
	}

	// checking availability
	availableStockForProduct, ok := w.MaxAvailableProducts[name]
	if !ok || availableStockForProduct < quantity {
		return errors.New("product requirement cannot be fulfilled with the current inventory")
	}

	// re-adjusting inventory
	w.AdjustInventory(quantity, w.ProductCatalogue[name].ContainingArticles)
	w.ProductsSold[name] = w.ProductsSold[name] + quantity // even if the product is sold for the first time, the map access will return 0
	fmt.Println(ColorBlue, fmt.Sprintf("Successfully sold %d nos of the product %s", quantity, name))
	fmt.Println()
	return nil
}

// getNameAndQuantityFromUser gets name and quantity of the product to be sold from the user
func getNameAndQuantityFromUser(existingProducts map[string]Product) (string, int64, error) {
	// getting name
	fmt.Println(ColorGreen, "Please enter the product name")
	reader := bufio.NewReader(os.Stdin)
	name, err := reader.ReadString('\n')
	if err != nil {
		return "", 0, errors.New(fmt.Sprintf("Sorry. There was an error while reading the input %v", err))
	}

	name = strings.Trim(name, "\n")
	_, ok := existingProducts[name]
	if !ok {
		return "", 0, errors.New("invalid product name")
	}

	// getting quantity
	fmt.Println(ColorGreen, "Please enter the quantity of the product you want to sell")
	var quantityString string
	_, err = fmt.Scanln(&quantityString)
	if err != nil {
		return "", 0, errors.New(fmt.Sprintf("Sorry. There was an error while reading the input %v", err))
	}

	// parsing quantity into int
	quantity, err := strconv.ParseInt(quantityString, 10, 64)
	if err != nil {
		return "", 0, err
	}

	return name, quantity, nil
}

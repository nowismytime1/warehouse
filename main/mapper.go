package main

import (
	"fmt"
	"strconv"
)

// MapProductFromModel maps product from input file model to entity
func MapProductFromModel(model ProductModel) (Product, error) {
	// converting price from string input to float
	var price float64
	var err error
	if model.Price != nil {
		price, err = strconv.ParseFloat(*model.Price, 64)
		if err != nil {
			return Product{}, err
		}
	}

	// mapping containing articles
	mappedContainingArticles, err := mapContainingArticlesFromModel(model.ContainingArticles)
	if err != nil {
		return Product{}, nil
	}

	return Product{
		Name:               model.Name,
		Price:              price,
		ContainingArticles: mappedContainingArticles,
	}, nil
}

// MapProductToModel maps product from entity to input file model
func MapProductToModel(entity Product) ProductModel {
	priceString := fmt.Sprintf("%f", entity.Price)
	return ProductModel{
		Name:               entity.Name,
		Price:              &priceString,
		ContainingArticles: mapContainingArticlesToModel(entity.ContainingArticles),
	}
}

// MapArticleFromModel maps article from input file model to entity
func MapArticleFromModel(model ArticleModel) (Article, error) {
	// converting quantity in stock from string to int
	quantityInStock, err := strconv.ParseInt(model.QuantityInStock, 10, 64)
	if err != nil {
		return Article{}, err
	}

	return Article{
		ID:              model.ID,
		Name:            model.Name,
		QuantityInStock: quantityInStock,
	}, nil
}

// MapArticleToModel maps article from entity to input file model
func MapArticleToModel(entity Article) ArticleModel {
	return ArticleModel{
		ID:              entity.ID,
		Name:            entity.Name,
		QuantityInStock: fmt.Sprintf("%d", entity.QuantityInStock),
	}
}

// mapContainingArticlesFromModel maps containingArticles from input file model to entity
func mapContainingArticlesFromModel(model []ProductArticleModel) ([]ProductArticle, error) {
	var containingArticles []ProductArticle
	for _, article := range model {
		mappedArticle, err := mapProductArticleFromModel(article)
		if err != nil {
			return nil, err
		}

		containingArticles = append(containingArticles, mappedArticle)
	}

	return containingArticles, nil
}

// mapProductArticleFromModel maps productArticle from input file model to entity
func mapProductArticleFromModel(model ProductArticleModel) (ProductArticle, error) {
	// converting quantityRequired from string to int
	quantityRequired, err := strconv.ParseInt(model.QuantityRequired, 10, 64)
	if err != nil {
		return ProductArticle{}, err
	}

	return ProductArticle{
		ArticleID:        model.ArticleID,
		QuantityRequired: quantityRequired,
	}, err
}

// mapContainingArticlesToModel maps containingArticles from entity to input file model
func mapContainingArticlesToModel(entity []ProductArticle) []ProductArticleModel {
	var containingArticles []ProductArticleModel
	for _, article := range entity {
		containingArticles = append(containingArticles, ProductArticleModel{
			ArticleID:        article.ArticleID,
			QuantityRequired: fmt.Sprintf("%d", article.QuantityRequired),
		})
	}

	return containingArticles
}

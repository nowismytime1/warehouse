package main

// InventoryModel replicates the encapsulating entity of articles in the input file
type InventoryModel struct {
	Articles []ArticleModel `json:"inventory"`
}

// ArticleModel replicates the article entity in the input file
type ArticleModel struct {
	ID              string `json:"art_id"`
	Name            string `json:"name"`
	QuantityInStock string `json:"stock"`
}

// ProductCatalogueModel replicates the encapsulating entity of products in the input file
type ProductCatalogueModel struct {
	Products []ProductModel `json:"products"`
}

// ProductModel replicates the product entity in the input file
type ProductModel struct {
	Name               string                `json:"name"`
	Price              *string               `json:"price,omitempty"`
	ContainingArticles []ProductArticleModel `json:"contain_articles"`
}

// ProductArticleModel replicates the building block of a product in the input file
type ProductArticleModel struct {
	ArticleID        string `json:"art_id"`
	QuantityRequired string `json:"amount_of"`
}

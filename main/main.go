package main

import (
	"fmt"
)

// main function where the program starts
func main() {
	warehouse, err := initialise()
	if err != nil {
		fmt.Println(ColorRed, fmt.Sprintf("Error while initialising the program: %v", err))
	}

	fmt.Println()
	fmt.Println(ColorYellow, "Welcome to IKEA warehouse...")
	fmt.Println()

	for {
		userInput := getUserInput()
		fmt.Println()
		callHandler(warehouse, userInput)

		if userInput == "7" || userInput == "8" {
			return
		}

	}
}

// initialise method carries out initialising tasks for the program
func initialise() (*Warehouse, error) {
	warehouse := &Warehouse{
		Inventory:            make(map[string]Article),
		ProductCatalogue:     make(map[string]Product),
		MaxAvailableProducts: make(map[string]int64),
		ProductsSold:         make(map[string]int64),
	}

	err := warehouse.InitializeFromFile()
	if err != nil {
		return nil, err
	}

	return warehouse, nil
}

// callHandler calls different handlers based on the user input
func callHandler(warehouse *Warehouse, userInput string) {
	var err error
	switch userInput {
	case "1":
		err = warehouse.LoadFreshInventory()
	case "2":
		err = warehouse.LoadIncrementalInventory()
	case "3":
		err = warehouse.LoadProductCatalogue()
	case "4":
		warehouse.ListProductsInInventory()
	case "5":
		err = warehouse.SellProduct()
	case "6":
		warehouse.ListSoldProducts()
	case "7":
		err = warehouse.SaveOnExit()
	}

	if err != nil {
		fmt.Println(ColorRed, fmt.Sprintf("Error while executing function, %v", err))
		fmt.Println(ColorRed, "Please try again")
	}
}

// getUserInput gets the user input for the operation to perform
func getUserInput() string {
	fmt.Println(ColorGreen, "Please select your option.")
	fmt.Println(ColorGreen, "1. Load fresh inventory from file.")
	fmt.Println(ColorGreen, "2. Load incremental inventory from file.")
	fmt.Println(ColorGreen, "3. Load product catalogue from file.")
	fmt.Println(ColorGreen, "4. List maximum available products in inventory. (Note: You might not be able to order all the products in display because the same articles can be used to build multiple products)")
	fmt.Println(ColorGreen, "5. Sell a product.")
	fmt.Println(ColorGreen, "6. List sold products.")
	fmt.Println(ColorGreen, "7. Save & Exit.")
	fmt.Println(ColorGreen, "8. Exit without saving.")

	var input string
	_, err := fmt.Scanln(&input)
	if err != nil || input < "1" || input > "8" {
		fmt.Println(ColorRed, "Sorry. The input is invalid. Please try again.")
		getUserInput()
	}

	return input
}

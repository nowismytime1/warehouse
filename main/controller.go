package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math"
	"os"
)

// LoadInventory is the controller method that performs all the operations needed to load an inventory
func (w *Warehouse) LoadInventoryController(inputFilePath string, isIncremental bool) error {
	// Re-initialising map if inventory is from scratch
	if !isIncremental {
		w.Inventory = make(map[string]Article)
	}

	// Reading from file
	file, err := ioutil.ReadFile(inputFilePath)
	if err != nil {
		return errors.New(fmt.Sprintf("Error while reading file: %v", err))
	}

	// unmarshalling into entity
	inventory := InventoryModel{}
	err = json.Unmarshal(file, &inventory)
	if err != nil {
		return errors.New(fmt.Sprintf("Error while unmarshalling into inventory: %v", err))
	}

	// creating the inventory for easy access through map
	for _, article := range inventory.Articles {
		mappedArticle, err := MapArticleFromModel(article)
		if err != nil {
			return err
		}

		if existingArticle, ok := w.Inventory[article.ID]; ok && isIncremental {
			mappedArticle.QuantityInStock += existingArticle.QuantityInStock
			w.Inventory[article.ID] = mappedArticle
		} else {
			w.Inventory[article.ID] = mappedArticle
		}
	}

	return nil
}

// LoadProductCatalogue is the controller method that performs all the operations needed to load product catalogue
func (w *Warehouse) LoadProductCatalogueController(inputFilePath string) error {
	// reading the file
	file, err := ioutil.ReadFile(inputFilePath)
	if err != nil {
		return errors.New(fmt.Sprintf("Error while reading file: %v", err))
	}

	// unmarshalling into entity
	productCatalogue := ProductCatalogueModel{}
	err = json.Unmarshal(file, &productCatalogue)
	if err != nil {
		return errors.New(fmt.Sprintf("Error while unmarshalling into productCatalogue: %v", err))
	}

	// creating the catalogue for easy access through map
	for _, product := range productCatalogue.Products {
		mappedProduct, err := MapProductFromModel(product)
		if err != nil {
			return err
		}

		w.ProductCatalogue[product.Name] = mappedProduct
	}

	return nil
}

// FindMaxAvailableProducts calculates maximum available products from the given inventory
func (w *Warehouse) FindMaxAvailableProducts() map[string]int64 {
	maxAvailableProducts := make(map[string]int64)
	for name, product := range w.ProductCatalogue {
		itemCount := int64(math.MaxInt64)
		for _, productArticle := range product.ContainingArticles {
			possibleCount := w.Inventory[productArticle.ArticleID].QuantityInStock / productArticle.QuantityRequired
			if possibleCount < itemCount {
				itemCount = possibleCount
			}
		}

		maxAvailableProducts[name] = itemCount
	}

	return maxAvailableProducts
}

// AdjustInventory removes the articles from the inventory and recalculates the maximum available products
func (w *Warehouse) AdjustInventory(quantitySold int64, articlesInProduct []ProductArticle) {
	for _, productArticle := range articlesInProduct {
		remainingQuantity := w.Inventory[productArticle.ArticleID].QuantityInStock - quantitySold*productArticle.QuantityRequired
		w.Inventory[productArticle.ArticleID] = Article{
			ID:              productArticle.ArticleID,
			Name:            w.Inventory[productArticle.ArticleID].Name,
			QuantityInStock: remainingQuantity,
		}
	}

	w.MaxAvailableProducts = w.FindMaxAvailableProducts()
}

// SaveJSONToFile converts the given object and saves it to file
func (w *Warehouse) SaveJSONToFile(object interface{}, filepath string) error {
	jsonString, err := json.Marshal(object)
	if err != nil {
		return errors.New(fmt.Sprintf("error while marshaling object: %v", err))
	}

	err = ioutil.WriteFile(filepath, jsonString, os.ModePerm)
	if err != nil {
		return errors.New(fmt.Sprintf("error while writing to file: %v", err))
	}

	return nil
}

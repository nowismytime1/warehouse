package main

// Article holds the attributes of a single article
type Article struct {
	ID              string
	Name            string
	QuantityInStock int64
}

// Product holds the attributes of a single product
type Product struct {
	Name               string
	Price              float64
	ContainingArticles []ProductArticle
}

// ProductArticle is the building block of a product
type ProductArticle struct {
	ArticleID        string
	QuantityRequired int64
}

// Warehouse resembles the complete database
type Warehouse struct {
	Inventory            map[string]Article
	ProductCatalogue     map[string]Product
	MaxAvailableProducts map[string]int64
	ProductsSold         map[string]int64
}

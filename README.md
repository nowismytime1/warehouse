# README #

Welcome to the IKEA warehouse. This program is organised into multiple layers. 
The program starts in the main.go file which is the driver file for the program. 
It takes the user input for the operations and calls the specific methods in handler.go file.
The methods in handler.go make use of business logic defined in controller.go to carry out the tasks.
We also have helper files like entity.go which stores the entity definition, 
model.go which stores the data representation at disk, 
constants.go which stores the program constants,
and mapper.go which has methods to transform between program entity and disk models.

To run the program, please execute:
* make run

The program has 8 operations:
* Load fresh inventory from file: This loads inventory from file from scratch. 
All previous inventory is discarded.
* Load incremental inventory from file: This loads inventory from file by 
appending the currently available inventory.
* Load product catalogue from file: This loads the product catalogue from the file
* List maximum available products in inventory: This lists maximum available quantity of each product. 
Note: You might not be able to order all the products in display because the same articles can be used to build multiple products)")
* Sell a product: This takes in a product name and quantity to be sold and sells the product 
and adjusts the inventory accordingly.
* List sold products: This lists all the products sold in the current session.
* Save & Exit: Exits by saving the current inventory and product catalogue to file
* Exit without saving: Exits the program without saving.

Each file/layer can be tested separately. Could not get time to write UTs for them.